import itertools
import pandas as pd
import numpy as np
import yfinance as yf

def calculate_returns(basket: list, data_windows: str, log_return: bool = True, cross_return: bool = False) -> pd.DataFrame:
    """
    Download Yahoo data and calculate returns
    API data.
    """
    ret_basket = pd.DataFrame()
    prices = pd.DataFrame()

    for ccy in basket:
        if log_return==True:

            price = yf.Ticker(ccy).history(period=data_windows)["Close"]
            ret = np.log(1 + price.pct_change().fillna(0))
        else:
            price = yf.Ticker(ccy).history(period=data_windows)["Close"]
            ret = ((price / price.shift(1)) - 1).fillna(0)

        ret_basket = pd.concat([ret_basket, ret], axis=1)
        prices = pd.concat([prices, price], axis=1)
    ret_basket.columns = basket
    prices.columns = basket

    if cross_return ==True:
        # create all combinations
        crosses = list(itertools.combinations(prices.columns, 2))
        for cross in crosses:
            prices[cross[0][:3] + cross[1][:3]] = prices[cross[0]] / prices[cross[1]]
            if log_return == True:
                ret = ret_basket[cross[0][0:-4]+'-USD'] - ret_basket[cross[1][0:-4]+'-USD']
                ret.name = cross[0][0:-4] + cross[1][0:-4]
            else:
                ret = (1+ ret_basket[cross[0][0:-4]+'-USD']) / (1+ ret_basket[cross[1][0:-4]+'-USD'])-1
                ret.name = cross[0][0:-4] + cross[1][0:-4]
            ret_basket = pd.concat([ret_basket, ret], axis=1)

    return ret_basket.fillna(0)

