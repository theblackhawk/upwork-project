## How to test the lambda locally 

In the root folder of your repo execute this commad to build the image

```bash
$ docker build -t financeproject:latest .
```

Then run the container using

```bash
$ docker run -p 9000:8080  financeproject:latest 
```

In another terminal execute this curl command to trigger the local lambda execution

```bash
curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{}'
```

You'll notice the logs in the first terminal

## How to push the docker container to AWS And have the lambda available publicly

You can find instructions on the following link : https://repost.aws/knowledge-center/lambda-container-images

Further if you want your lambda to execute on a given schedule check this link : https://docs.aws.amazon.com/eventbridge/latest/userguide/eb-run-lambda-schedule.html
