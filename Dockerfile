# Docker Context will be : .

FROM public.ecr.aws/lambda/python:3.8

COPY requirements.txt  ${LAMBDA_TASK_ROOT}/requirements.txt
RUN python -m pip install -r ${LAMBDA_TASK_ROOT}/requirements.txt

COPY ./rates ${LAMBDA_TASK_ROOT}/rates

COPY ./index.py ${LAMBDA_TASK_ROOT}/index.py

CMD [ "index.lambda_handler" ]