import time
import pandas as pd
from pandas import json_normalize
from datetime import datetime
import numpy as np

from dydx3 import Client
import logging
import csv
import sys
from dydx3.constants import *
import pandas as pd
import numpy as np
import copy
import itertools
from typing import Tuple
import yfinance as yf
from sklearn.covariance import LedoitWolf

logging.basicConfig(filename="execution.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')

# Let us Create an object
logger = logging.getLogger()

# Now we are going to Set the threshold of logger to DEBUG
logger.setLevel(logging.DEBUG)

def mktinfo(mkt):
    while True:
        try:
            client = Client(host="https://api.dydx.exchange")
            logger.debug("run client.public.get_markets()")
            markets = client.public.get_markets()
            break
        except ConnectionError:
            # if time.time() > start_time + connection_timeout:
            raise Exception('Unable to get updates after {} seconds of ConnectionErrors')
            # else:
                # time.sleep(1)  # attempting once every second
    logger.debug("get market data from mdf")
    mdf = json_normalize(markets.data)

    ticksize = mdf['markets.' + mkt + '.tickSize'][0]
    minordersize = mdf['markets.' + mkt + '.minOrderSize'][0]
    status = mdf['markets.' + mkt + '.status'][0]
    volume24H = mdf['markets.' + mkt + '.volume24H'][0]
    trades24H = mdf['markets.' + mkt + '.trades24H'][0]
    openInterest = mdf['markets.' + mkt + '.openInterest'][0]
    indexPrice = mdf['markets.' + mkt + '.indexPrice'][0]
    oraclePrice = mdf['markets.' + mkt + '.oraclePrice'][0]
    stepSize = mdf['markets.' + mkt + '.stepSize'][0]

    return ticksize, minordersize, status, volume24H, trades24H, openInterest, indexPrice, oraclePrice, stepSize

def lambda_handler(event, context):

    # Permanently changes the pandas settings
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    pd.set_option('display.max_colwidth', None)

    sys.path.append('bt')
    from rates .rates import calculate_returns

    basket_1 = ["ETH-USD", "BTC-USD", "YFI-USD"]
    basket_2 = ["SOL-USD", "ICP-USD", "NEAR-USD", "ETC-USD"]
    basket_3 = ["ADA-USD"]
    basket_ccys = {'basket_main': basket_1, 'basket_intermediate': basket_2, 'basket_small': basket_3}
    rets = {}
    for basket in basket_ccys:
        ret = calculate_returns(basket_ccys[basket], "1mo", True, False).reindex(calculate_returns(['BTC-USD'], "1mo", True, False).index)
        rets[basket] = ret
        for c in basket:
            try:
                ticksize, minordersize, status, volume24H, trades24H, openInterest, indexPrice, oraclePrice, stepSize = mktinfo(c)
                print(c,ticksize, minordersize, status, volume24H, trades24H, openInterest, indexPrice, oraclePrice, stepSize)
            except Exception as e:
                print(e)
                continue

    return rets

if __name__ == '__main__':
    lambda_handler("","")